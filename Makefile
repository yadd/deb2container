# Makefile to install deb2container.
# required: bash
#
# just type: sudo make install

all:
	# nothing to do. Please use: sudo make install

install:
	install -D bin/deb2docker $(DESTDIR)$(prefix)/usr/bin/deb2docker
	install -D bin/deb2apptainer $(DESTDIR)$(prefix)/usr/bin/deb2apptainer

uninstall:
	-rm -f $(DESTDIR)$(prefix)/usr/bin/deb2docker
	-rm -f $(DESTDIR)$(prefix)/usr/bin/deb2apptainer
	
deb:
	debuild -b

.PHONY: install uninstall deb

