# deb2container

A set of tools to automatically port Debian packages into containers such as Docker and Apptainer/Singularity.

## Purpose

These tools are meant to ease the installation of Debian packages into containers such as [Docker](https://www.docker.com/) and [Apptainer/Singularity](https://apptainer.org/). This way, it is straight forward to push a set of Debian packages into containers that can be deployed anywhere, e.g.:

```
deb2apptainer imagej
```

will create an image holding ImageJ. Then you may simply start the container with, e.g.:
```
/tmp/deb2apptainer-ixc6f7VN/imagej.sif         # to get a bash shell in which you can launch imagej
/tmp/deb2apptainer-ixc6f7VN/imagej.sif imagej  # to directly launch imagej
```

In addition, Desktop launchers and the `build` and `start` helper scripts are created in the target directory.

The `deb2apptainer` and `deb2docker` tools use the same syntax, e.g.:
```
Usage: deb2apptainer [options] packages...
  -B        do NOT build the image (default is to build)
  -c CMD    command to run in the container (default to /bin/bash)
  -f FROM   indicate which distribution is to be used (default to debian:stable)
  -h        show this help
  -n NAME   name of the image (default to package list)
  -o DIR    use given directory for the build (default is in /tmp)
  -s SCRIPT execute the given script during the container build
  The package list can be any Debian package, as well as local .deb
```

The options allow to tune the image, especially the distribution to use (Debian based, from Docker), the target build directory, and the default command to execute. This latter can be specified when launching the container itself, as in the examples. You may also specify a user script to be inserted in the build process. The default target directory `DIR` is a random name in `/tmp`.

A `start` script is written in the target directory `DIR`, which additional arguments are the command to execute. The `-B` option disables the image creation (in which case you should launch the `build` script yourself).

When Desktop launcher files are found in the Debian packages, they are updated in `DIR/launchers` to launch the applications from the host system directly calling the container. Any corresponding icon is also stored in a `DIR/icon`. You will have to adapt these launchers in case you relocate the container.


## Installation

The best way for installation is to use the `deb2container` Debian package. However, you may as well install the tools manually with any of:
```
sudo make install
sudo cp bin/deb2* /usr/bin/
```
or even use the tools directly, without requiring administrator rights.
```
bin/deb2apptainer pkg1 pkg2 ...
```

--------------------------------------------------------------------------------

## Usage: deb2apptainer to build Singulatiry/Apptainer images

The `deb2apptainer` tool will install Debian packages into a Singularity/Apptainer image.
An Apptainer/Singularity container is created locally. 

The main advantages of Apptainer wrt Docker are that the container is ran from the user directory, stands in a single file, does not run as root, and exports X11.

```
Usage: debapptainer [-hB][-f FROM][-c CMD][-n NAME][-o DIR][-s SCRIPT] packages...
```

The command used to manually build the image is:
```
cd /path/to/IMAGE-NAME
apptainer build IMAGE-NAME.def
```

The command used to manually launch a contained from the built image is:
```
apptainer run  IMAGE-NAME
apptainer exec IMAGE-NAME bash
```

A typical example is:

- `deb2apptainer -o /tmp/apptainer-xeyes x11-apps`
- `/tmp/apptainer-xeyes/start xeyes` will start *xeyes* directly.
- `/tmp/apptainer-xeyes/x11-apps.sif xeyes` does the same
- `/tmp/apptainer-xeyes/start` will bring a `bash` shell, in which you may type `xeyes`, and then `exit`.
- `/tmp/apptainer-xeyes/x11-apps.sif` does the same.

#### Apptainer/Singularity configuration

You obviously require to have `apptainer` installed. 

Get the Debian package at:
- https://apptainer.org/docs/admin/main/installation.html#install-debian-packages
- [apptainer_1.2.5_amd64.deb](https://github.com/apptainer/apptainer/releases/download/v1.2.5/apptainer_1.2.5_amd64.deb)

--------------------------------------------------------------------------------


## Usage: deb2docker to build docker images

The `deb2docker` tool will install Debian packages into a Docker image.
A Docker image is created.

```
Usage: deb2docker [-hB][-f FROM][-c CMD][-n NAME][-o DIR][-s SCRIPT] packages...
```

You may have to further tune the Desktop launchers, e.g. use `Exec=` lines such as:
```
Exec=sh -c "docker run -it --net=host --env=DISPLAY --env\='QT_X11_NO_MITSHM=1' --volume=\"$HOME/.Xauthority:/home/user/.Xauthority:rw\" IMAGE-NAME EXECUTABLE %F"
```
so that `$HOME` is properly passed to the X11 path.

:warning: In order for GUI to display, you may need to use an `xhost +` command.

The command used to manually build the image is:
```
docker build --rm -t IMAGE-NAME DIR
```

The command used to manually launch a contained from the built image is:
```
docker run --rm -it --net=host --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="$HOME/.Xauthority:/home/user/.Xauthority:rw" IMAGE-NAME
```

A typical example is:

- `deb2docker -o /tmp/docker-xeyes x11-apps`
- `/tmp/docker-xeyes/start xeyes` will start *xeyes* directly.
- `/tmp/docker-xeyes/start` will bring a `bash` shell, in which you may type `xeyes`, and then `exit`.

#### Docker configuration

You need of course to have Docker installed and be part of the `docker` group:
```
sudo apt install docker.io
sudo usermod -aG docker $USER
```

If you are behind a proxy, you may have to write a `~/.docker/config.json` file:
```json
{
 "proxies":
  {
    "default":
    {
      "httpProxy":"http://195.221.0.35:8080",
      "httpsProxy":"http://195.221.0.35:8080"
    }
  }
}
```

At the system level, you should also configure the proxy:
```
sudo mkdir -p /etc/systemd/system/docker.service.d
sudo nano /etc/systemd/system/docker.service.d/http-proxy.conf
```
and wite content:
```
[Service]
Environment="HTTP_PROXY=http://195.221.0.35:8080"
Environment="HTTPS_PROXY=http://195.221.0.35:8080"
```

Usual commands to handle Docker containers are:

- build: `docker build --rm Dockerfile`
- run:   `docker run   --rm -it NAME`
- clean:` docker rmi NAME`
- clean ALL: `docker system prune -a`

## Local Debian packages

You may create a local Debian package with:
```
sudo apt install devscripts
make deb
```

## Credits

(c) E. Farhi, Synchrotron SOLEIL - 2024 - GPL3.

Other tools that you may also use:

- [distrobox](https://github.com/89luca89/distrobox)

